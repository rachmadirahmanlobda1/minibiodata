package com.biodata.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.biodata.services.jobServie;

@Controller
@RequestMapping(value = "/job")
public class jobController {
	@GetMapping("/")
    public String jobs(){

        jobServie.jobSat();

        return "jobs";
    }
}
