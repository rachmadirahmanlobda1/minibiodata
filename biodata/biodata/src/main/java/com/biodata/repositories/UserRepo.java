package com.biodata.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.biodata.model.Users;

@Repository
public interface UserRepo extends JpaRepository<Users, Long> {
    @Query(value = "SELECT * FROM users u WHERE u.username=?1 AND u.password=?2", nativeQuery = true)
    java.util.List<Users> getLogin(String Username, String Password);

	
}
