package com.biodata.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.biodata.model.job;

@Repository
public interface jobRepo extends JpaRepository<job, Long>{

}
